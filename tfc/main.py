#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 15 15:52:29 2023

@author: amonteil
"""
import typer
import tfc.script.US_0
import tfc.script.US_1
import tfc.script.genschema
import tfc.script.cleverCSV
import tfc.script.check


app = typer.Typer()
app.command()(tfc.script.US_0.csv_checker)
app.command()(tfc.script.US_1.header_checker)
app.command()(tfc.script.genschema.genschema)
app.command()(tfc.script.cleverCSV.cleverCSV)
app.command()(tfc.script.check.check)


if __name__ == '__main__':
    app()
