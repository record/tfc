#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 12 16:49:36 2023

@author: amonteil
"""
import json
from frictionless import describe
from typing_extensions import Annotated
import typer
from tfc.script.utils import add_field_json, dict_to_json
from tfc.script.utils import constraints_parser, write_dict_list
from tfc.script.utils import validate_constraints, mandatories_parser
from tfc.script.utils import schema_reorganization


app = typer.Typer()


valid_constraints = ['begin_date_field', 'regex', 'in', '<', '>', 'function']


def genschema(file: Annotated[str,
              typer.Argument(help="File name or Path/filename")],
              mandatories: Annotated[str,
              typer.Option(help=("list containning mandatories fields name. "
                                 "\n\nExample: 'field_1,...,field_n'"))
              ] = '',
              verbose: Annotated[bool, typer.Option()] = False,
              atomic: Annotated[str,
              typer.Option(help="list containning atomic constraints"
                                "\n\nExample: '[field_1,constraint_1];...;"
                                "[field_n,constraint_n]'")] = '',
              columns: Annotated[str,
              typer.Option(help="list containning columns constraints\n\n"
                                "Example: '[field_a1,field_b1,constraint_1],"
                                "[field_a2,constraint_2],..."
                                "[field_ap,constraint_p,operator_p,value_p],"
                                "...,[field_an,field_bn,constraint_n]'")
              ] = '',
              schema_name: Annotated[str,
              typer.Option(help="Where you want the schema to be created"
                                "Example: path/to/my/file")] = ''
              ):
    """
    Generate a schema of the file
    """
    description = describe(file)
    description.infer(stats=True)
    header = description.header
    description_name = file.replace('.csv', '_schema.json')
    if schema_name:
        description_name = schema_name
    description.to_json(description_name)
    add_field_json(description_name, 'header', header)
    with open(description_name) as json_file:
        description = json.load(json_file)
    schema = schema_reorganization(description['schema'])
    for field in schema:
        schema[field]['mandatory'] = False
    description['schema']['fields'] = schema
    if mandatories:
        mandatories = mandatories_parser(mandatories)
        for mandatory in mandatories:
            description['schema']['fields'][mandatory]['mandatory'] = True
    if atomic or columns:
        description['constraints'] = {}
    if atomic:
        atomic = constraints_parser(atomic)
        state, invalid_constraints = validate_constraints(atomic,
                                                          valid_constraints)
        if not state:
            return f"{invalid_constraints} are not valid constraints"
        for element in atomic:
            if len(element) == 2:
                field, constraint = element
                write_dict_list(description['constraints'],
                                'atomic',
                                (field, constraint)
                                )
            elif len(element) == 3:
                field, operator, value = element
                write_dict_list(description['constraints'],
                                'atomic',
                                (field, operator, value)
                                )
    if columns:
        columns = constraints_parser(columns)
        state, invalid_constraints = validate_constraints(columns,
                                                          valid_constraints)
        if not state:
            return f"{invalid_constraints} are not valid constraints"
        for element in columns:
            if len(element) == 3:
                field1, constraint, field2 = element
                write_dict_list(description['constraints'],
                                'columns',
                                (field1, constraint, field2))
            if len(element) == 4:
                field1, constraint, function, field2 = element
                write_dict_list(description['constraints'],
                                'columns',
                                (field1, constraint, function, field2))
    dict_to_json(description, description_name)
    print(f"json file created at  : {description_name}\n")
    if verbose:
        print(f"description : \n{description}\n")
    return description, description_name
