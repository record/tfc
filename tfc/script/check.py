#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 15 16:24:27 2023

@author: amonteil
"""
import typer
from typing_extensions import Annotated
from tfc.script.genschema import genschema
from tfc.script.utils import dict_to_json, open_json, list_equality
from tfc.script.utils import mandatory_field_check, schema_check
from tfc.script.utils import constraints_check


app = typer.Typer()


def open_file(file: Annotated[str,
              typer.Argument(help="CSV File name or Path/filename"
                                  "whose you want to check schema")]
              ):
    """
    Generate a dict containing the schema of the argument's file
    and generate a list of the dict keys

    Parameters
    ----------
    file : str
        CSV File name or Path/filename whose you want to check schema

    Returns
    -------
    file_description : dict
        dict containing the schema of the argument's CSV file.
    file_description_keys : list
        list containing the keys of the dict.

    """
    file_description = genschema(file, schema_name="temp.json")[0]
    file_description_keys = list(file_description.keys())
    file_description_keys.remove("name")
    file_description_keys.remove("path")
    file_description_keys.remove("hash")
    return file_description, file_description_keys


def check(file: Annotated[str,
          typer.Argument(help="File name or Path/filename"
                              "whose you want to check schema")],
          jsonfile_ref: Annotated[str,
          typer.Argument(help="File name or Path/filename"
                              "which is ref schema")],
          ordered: Annotated[bool,
          typer.Option(help="check the colunm s order")] = False,
          extra_col: Annotated[bool,
          typer.Option(help="authorize extra columns in file"
                            " compared to schema")] = True,
          verbose: Annotated[bool, typer.Option()] = False):
    """
    Check if a file match a schema
    """
    error = {}
    file_description, file_description_keys = open_file(file)
    ref, checks = open_json(jsonfile_ref)
    equality, extra, missing = list_equality(file_description["header"],
                                             ref["header"])
    keys_to_check = set(file_description_keys) & set(checks)
    option_validation = True

    for check in keys_to_check:
        if check == "header" and not equality:
            error["extra fields"] = extra
            error["missing fields"] = missing
        elif check == "schema":
            fields_to_check = (set(file_description["schema"]["fields"].keys())
                               & set(ref["schema"]["fields"].keys()))
            schema_check(file_description, ref, fields_to_check, error)
        elif check == "fields" and file_description["fields"] != ref["fields"]:
            error[check] = {f"wrong file {check} number ":
                            (f"file {check} number({file_description[check]}) "
                             f"does not match with ref ({ref[check]})")
                            }
        elif file_description[check] != ref[check]:
            error[check] = {f"wrong file {check}":
                            (f"file {check} ({file_description[check]}) does"
                             f" not match with ref ({ref[check]})")
                            }
    if 'constraints' in ref.keys():
        constraints_check(file, file_description, ref, error)
    if missing:
        mandatory_field_check(error, ref, missing)

    if ordered:
        error["column s order"] = file_description["header"] == ref["header"]

    if not extra_col and not equality and extra:
        option_validation = False
        error["forbidden extra fields"] = extra

    if verbose:
        print(error)

    if not error and option_validation:
        print("No error")
    else:
        print("error, json file created at: "
              f"{file.replace('.csv', '_report.json')}\n")
        dict_to_json(error, file.replace('.csv', '_report.json'))

    return error
