#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 10:37:33 2023

@author: amonteil
"""

from frictionless import describe
import typer
from typing_extensions import Annotated
from tfc.script.utils import write_dict_str


app = typer.Typer()


def header_checker(file: Annotated[str,
                   typer.Argument(help="File name or Path/filename")],
                   names: Annotated[str,
                   typer.Argument(help=("list containning each column s name. "
                                        "Example : [x1,x2,...,xn]")
                                  )] = '',
                   verbose: Annotated[bool, typer.Option()] = False):
    """Check file header and column s name"""

    meta = describe(file)
    validation = True
    error_msg = {"Header error": {}, "column s name error": {}}

    if not meta.dialect.header:  # trouver condition sur la doc
        validation = False
        error_msg["Header error"]["No Header"] = ("The file does not have "
                                                  "header")

    if meta.dialect.header_rows[0] != 1:  # verifier si condition est utile
        validation = False
        error_msg["Header error"]["postion"] = (f" No header in line 1, "
                                                "instead in line"
                                                f" {meta.dialect.header_rows}")

    for i in range(len(meta.header)):

        if meta.header[i] == f"field{i+1}":
            validation = False
            write_dict_str(error_msg["column s name error"], meta.header[i],
                           f"No column s name in column {i+1}")

        try:
            int(meta.header[i])

        except ValueError:
            None

        else:
            validation = False
            write_dict_str(error_msg["column s name error"], meta.header[i],
                           f" Numerical column s name in column {i}")

    if names:
        names = names.replace("[", "")
        names = names.replace("]", "")
        names = names.split(',')
        for i in range(len(names)):
            if meta.header[i] != names[i]:
                validation = False
                write_dict_str(error_msg["column s name error"],
                               meta.header[i],
                               f" column s {i+1} name does not match with"
                               f" argument list name ({names[i]})"
                               )
    if verbose:
        print(validation, error_msg)
    else:
        print(validation)
    return validation, error_msg
