#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 08:44:01 2023

@author: amonteil
"""


import json
import os
from frictionless import extract


def write_dict_str(dictionnary, key, val):
    """Write strings elements in a dict

    If the key exist add val, if not create the couple key : val
    All val are supposed to be strings

    Parameters
    ----------
    dictionnary : dict
        Dictionnary where you want to  add something.
    key : str
        Key you want to add.
    val : str
        String you want to add.

    Returns
    -------
    None.

    """
    if key in dictionnary.keys():
        dictionnary[key] += val
    else:
        dictionnary[key] = val


def write_dict_list(dictionnary, key, val):
    """Write lists elements in a dict

    If the key exist add val, if not create the couple key : val
    all val are supposed to be dict

    Parameters
    ----------
    dictionnary : dict
        Dictionnary where you want to  add something.
    key : TYPE
        Key you want to add.
    val : list
        List you want to add.

    Returns
    -------
    None.

    """
    if key in dictionnary.keys():
        dictionnary[key].append(val)
    else:
        dictionnary[key] = [val]


def add_field_json(jsonfile, key, value):
    """Add the couple key:value at the end of the jsonfile

    Parameters
    ----------
    jsonfile : str
        Filename.json or path/to/file/filename.json .
    key : str
        Key you want to add.
    value : str
        Value you want to add.

    Returns
    -------
    None.

    """
    with open(jsonfile) as file:
        jsonfile_dict = json.load(file)
    jsonfile_dict[key] = value
    with open(jsonfile, 'w') as file:
        json.dump(jsonfile_dict, file, indent=4)


def dict_to_json(dictionnary, filename):
    """Create a json file from a dict


    Parameters
    ----------
    dictionnary : dict
        Dictionnary you want to create a jsonfile from.
    filename : str
        What name the json file will have.

    Returns
    -------
    None.

    """
    if '.json' in filename:
        with open(filename, 'w') as file:
            json.dump(dictionnary, file, indent=4)
    else:
        with open(filename + '.json', 'w') as file:
            json.dump(dictionnary, file, indent=4)


def schema_reorganization(schema):
    """Copy and reorganize the schema

    Parameters
    ----------
    schema : dict
        Schema of a file, generated with genschema.

    Returns
    -------
    dictionnary : dict
        Dictionnary where the schema is reorganized.

    """
    dictionnary = {}
    for element in schema["fields"]:
        metadata = list(element.items())
        metadata.remove(('name', element['name']))
        dictionnary[element['name']] = dict(metadata)
    return dictionnary


def list_equality(L1, L2):
    """
    Check if all L1 elements are in L2, if all L2 elements are in L1,
    and if L1 == L2
    """
    InL = True
    InR = True
    Eq = False
    NotInL = []
    NotInR = []
    for element in L1:
        if element not in L2:
            InL = False
            NotInR.append(element)
    for element in L2:
        if element not in L1:
            InR = False
            NotInL.append(element)
    if InL and InR:
        Eq = True
    return Eq, NotInR, NotInL


def open_json(jsonfile):
    """
    Convert a json schema into dict
    and generate a list of the dict keys
    """
    with open(jsonfile, 'r') as file:
        ref = json.load(file)
    checks = list(ref.keys())
    checks.remove("name")
    checks.remove("path")
    checks.remove("bytes")
    if "hash" in checks:
        checks.remove("hash")
    return ref, checks


def mandatory_field_check(error_dict, ref_schema, missing_field):
    """
    check in the missing fields; which one are mandatory, and writing them
    in the error dictionnary
    """
    error_dict['mandatory field missing'] = []
    error_dict['non mandatory field missing'] = []
    schema = ref_schema['schema']['fields']
    for field in missing_field:
        if schema[field]['mandatory']:
            error_dict['mandatory field missing'].append(field)
        else:
            error_dict['non mandatory field missing'].append(field)


def schema_check(description, reference, fields_to_check, error_dict):
    """
    for each field check all the metadata between a file and a reference
    schema
    """
    for field in fields_to_check:
        for metadata in description["schema"]["fields"][field]:
            if metadata != "mandatory":
                if (description["schema"]["fields"][field][metadata]
                        != reference["schema"]["fields"][field][metadata]):
                    error_dict["field type"] = (
                        {field: (
                            f'{field} {metadata} :'
                            f'({description["schema"]["fields"][field][metadata]})'
                            ' does not match with ref '
                            f'({reference["schema"]["fields"][field][metadata]})'
                        )})


def get_date_fields(description):
    """
    get a list of all the date fields

    Parameters
    ----------
    description : dict
        dict from genschema.
    Returns
    -------
    date_fields : list
        list containning all the field which type is date.
    """
    date_fields = []
    for field in description['schema']['fields']:
        if description['schema']['fields'][field]['type'] == 'date':
            date_fields.append(field)
    return date_fields


def get_col(file, field):
    """
    extract columns

    Parameters
    ----------
    file : str
        filename or path/filename
    field : str
        name of the column you want to extract

    Returns
    -------
    col : list
        column extracted

    """
    extracted = extract(file)
    rows = extracted[list(extracted.keys())[0]]
    col = []
    for row in rows:
        col.append(row[field])
    return col


def date_check(date_min_field, date_fields, file, error_dict):
    """
    check if dates are consistent between eachother

    Parameters
    ----------
    date_min_field : str
        name of the date min field of the table
    date_fields : list
        list containning all date fields
    file : str
        filename or path/filename.
    error_dict : dict
        dictionnary containning the error of check function.

    Returns
    -------
    None.

    """
    refs = get_col(file, date_min_field)
    for field in date_fields:
        column = get_col(file, field)
        for ref, date in zip(refs, column):
            if date < ref:
                write_dict_list(error_dict['constraints'],
                                'atomic_constraints',
                                (f'inconsistent dates: {str(date)} in field '
                                 f'{field} is before date min: {str(ref)} in'
                                 f' field: {date_min_field}')
                                )


def constraints_parser(chain):
    """
    parse a formatted string into a list

    Parameters
    ----------
    chain : str
        string list you want to parse.
        example: '[el_1,...,el_n];...;[el1,...,el_n]'
    Returns
    -------
    chain : list
        parsed string into a list.
        example: [['el_1',...,'el_n'],...,['el_1',...,'el_n']]
    """
    chain = chain.replace(' ', '')
    chain = chain.split(';')
    for i in range(len(chain)):
        chain[i] = chain[i].replace('[', '')
        chain[i] = chain[i].replace(']', '')
        chain[i] = chain[i].split(',')
    return chain


def list_parser(chain):
    """
    parse a string list into a list

    Parameters
    ----------
    chain : str
        string list.
        example: '[element_1,...,element_n]'
    Returns
    -------
    chain : list
        parsed string list.
        example: ['element_1', ..., 'element_n']
    """
    chain = chain.replace(' ', '')
    chain = chain.replace('[', '')
    chain = chain.replace(']', '')
    chain = chain.split(',')
    return chain


def mandatories_parser(chain):
    chain = chain.replace(' ', '')
    chain = chain.split(',')
    return chain


def get_directory(filename):
    """
    get directory from a path/filename

    Parameters
    ----------
    filename : str
        filename or path/filename.

    Returns
    -------
    directory : str
        directory of the file.

    """
    directory = filename
    directory = directory.split('/')
    directory.pop()
    directory = '/'.join(directory)
    return directory


def in_directory(column, file):
    """
    In a column containing only files, check if these files are in the
    directory of the files containing the argument's column

    Parameters
    ----------
    column : list
        list containning all the cell of a column.
    file : str
        path/filename.

    Returns
    -------
    state : bool
        True if all the files are in the "mother file" directory.
    missing_files : list
        list containing all the files that are not in the "mother file"
        directory.

    """
    state = False
    missing_files = []
    directory = get_directory(file)
    file_in_directory = os.listdir(directory)
    for cell in column:
        if cell not in file_in_directory:
            state = False
            missing_files.append(cell)
    return state, missing_files


def regex_check(column, expression):
    for cell in column:
        None  # a faire plus tard


def greater_than(column_1, column_2):
    """
    check cell by cell if column_1 is greater than column_2

    Parameters
    ----------
    column_1 : list
        list representing a column of your table you want to check
    column_2 : list
        list representing a column of your table you want to check
    Returns
    -------
    bool
       True if column_1 is greater than column_2 cell by cell, otherwise False.
    i : int
       Index of the row where column_1 is not greater than column_2.

    """
    index = []
    state = True
    for i, (cell_1, cell_2) in enumerate(zip(column_1, column_2)):
        if cell_2 >= cell_1:
            index.append(i)
            state = False
    index = list(map(lambda x: x + 2, index))
    return state, index


def lower_than(column_1, column_2):
    """
    check cell by cell if column_1 is lower than column_2

    Parameters
    ----------
    column_1 : list
        list representing a column of your table you want to check
    column_2 : list
        list representing a column of your table you want to check
    Returns
    -------
    state : bool
        True if column_1 is lower than column_2 cell by cell, otherwise False.
    i : int
        Index of the row where column_1 is not lower than column_2.

    """
    index = []
    state = True
    for i, (cell_1, cell_2) in enumerate(zip(column_1, column_2)):
        if cell_2 <= cell_1:
            index.append(i)
            state = False
    index = list(map(lambda x: x + 2, index))
    return state, index


def function(column_1, f, column_2):
    """
    check cell by cell if column_2 = f(column_1)

    Parameters
    ----------
    column_1 : list
        list representing a column of your table you want to check
    column_2 : list
        list representing a column of your table you want to check
    f : str
        function.

    Returns
    -------
    bool
        True if column_1 is lower than column_2 cell by cell, otherwise False.
    i : int
        Index of the row where column_1 is not lower than column_2.

    """
    index_error = []
    state = True
    for i in range(len(column_1)):
        if column_2[i] != eval(f.replace('x', str(column_1[i]))):
            index_error.append(i)
            state = False
    index_error = list(map(lambda x: x + 2, index_error))
    return state, index_error


def atomic_lower_than(column, value):
    """
    check if every cell of the column is lower than value

    Parameters
    ----------
    column_1 : list
        list representing a column of your table you want to check
    column_2 : list
        list representing a column of your table you want to check
    f : str
        function.

    Returns
    -------
    bool
        True if every cell of column_1 is lower than value, otherwise False.
    i : int
        Index of the row where column_1 is not lower than value.

    """
    index_error = []
    state = True
    for index, element in enumerate(column):
        if element >= eval(value):
            index_error.append(index)
            state = False
    index_error = list(map(lambda x: x + 2, index_error))
    return state, index_error


def atomic_greater_than(column, value):
    """
    check if every cell of the column is greater than value

    Parameters
    ----------
    column_1 : list
        list representing a column of your table you want to check
    column_2 : list
        list representing a column of your table you want to check
    f : str
        function.

    Returns
    -------
    bool
        True if every cell of column_1 is greater than value, otherwise False.
    i : int
        Index of the row where column_1 is not lower than column_2.

    """
    index_error = []
    state = True
    for index, element in enumerate(column):
        if element <= eval(value):
            index_error.append(index)
            state = False
    index_error = list(map(lambda x: x + 2, index_error))
    return state, index_error


def constraints_check(file, description, ref, error):
    """
    check if description follows ref constraints, if not write the errors in
    error dict.
    Parameters
    ----------
    file : str
        filename or path/filenale.
    description : dict
        dict from genschema.
    ref : dict
        dict from genschema.
    error : dict
        dict containning all the errors in check functions.

    Returns
    -------
    None.

    """
    error['constraints'] = {}
    if 'atomic_constraints' in ref['constraints'].keys():
        error['constraints']['atomic_constraints'] = []
        for element in ref['constraints']['atomic_constraints']:
            if len(element) == 2:
                field, constraint = element
                if constraint == 'begin_date_field':
                    date_fields = get_date_fields(description)
                    date_check(field, date_fields, file, error)
                elif constraint == 'regex':
                    None
                elif constraint == 'in':
                    state, missings_files = in_directory(get_col(file, field),
                                                         file)

                    if not state and missings_files:
                        write_dict_list(error['constraints'],
                                        'atomic_constraints',
                                        f"{missings_files} are missing ")

            elif len(element) == 3:
                field, operator, value = element
                column = get_col(file, field)
                if operator == '<':
                    state, index = atomic_lower_than(column, value)
                    if not state:
                        write_dict_list(error['constraints'],
                                        'atomic_constraints',
                                        f"{field} is greater than {value}"
                                        f" at rows: {index}")
                elif operator == '>':
                    state, index = atomic_greater_than(column, value)
                    if not state:
                        write_dict_list(error['constraints'],
                                        'atomic_constraints',
                                        f"{field} is lower than {value}"
                                        f" at rows: {index}")
        if not error['constraints']['atomic_constraints']:
            del error['constraints']['atomic_constraints']

    if 'columns_constraints' in ref['constraints'].keys():
        error['constraints']['columns_constraints'] = []
        for element in ref['constraints']['columns_constraints']:
            if len(element) == 3:
                field_1, constraint, field_2 = element
                column_1 = get_col(file, field_1)
                column_2 = get_col(file, field_2)
                if constraint == '<':
                    state, index = lower_than(column_1, column_2)
                    if not state:
                        write_dict_list(error['constraints'],
                                        'columns_constraints',
                                        f"{field_1} is not lower than "
                                        f"{field_2} at row {index}")
                elif constraint == '>':
                    state, index = greater_than(column_1, column_2)
                    if not state:
                        write_dict_list(error['constraints'],
                                        'columns_constraints',
                                        f"{field_1} is not greater than "
                                        f"{field_2} at row {index}")
            elif len(element) == 4:
                field_1, constraint, expression, field_2 = element
                column_1 = get_col(file, field_1)
                column_2 = get_col(file, field_2)
                if constraint == 'function':
                    state, index = function(column_1,
                                            expression,
                                            column_2)
                    if not state:
                        write_dict_list(error['constraints'],
                                        'columns_constraints',
                                        f"{field_1} are not matching {field_2}"
                                        f" formula ({expression}) at row "
                                        f"{index}")
            else:
                return 'Error'
        if not error['constraints']['columns_constraints']:
            del error['constraints']['columns_constraints']

    if not error['constraints']:
        del error['constraints']


def validate_constraints(constraints, valid):
    state = True
    wrong_constraints = []
    for constraint in constraints:
        if len(constraint) == 2 and constraint[1] not in valid:
            state = False
            wrong_constraints.append(constraint)
        elif len(constraint) == 3 and constraint[1] not in valid:
            state = False
            wrong_constraints.append(constraint[1])
        elif len(constraint) == 4 and constraint[1] not in valid:
            state = False
            wrong_constraints.append(constraint[1])
    return state, wrong_constraints
