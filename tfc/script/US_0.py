#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 16:32:29 2023

@author: amonteil
"""
from frictionless import describe
import clevercsv
import typer


app = typer.Typer()


def csv_checker(file: str):
    """Check if the file format is csv, with RFC 4180 norm.\n
       If not create a new file complying with RFC 4180 norm.\n
       Return :\n
       validation: Bool   Describe if the file is valid \n
       error_msg: str     Give you the the reasons of the invalidity \n
    """

    error_msg = ""
    validation = True
    metadata = describe(file)
    file_format = metadata.format
    delimiter = metadata.dialect.get_control(file_format).delimiter

    if file_format != "csv":
        error_msg += " wrong format "
        validation = False

    if delimiter != ",":
        error_msg += " wrong delimiter "
        validation = False

    if not validation:  # TODO maybe replace this section by cleverCSV(file)
        new_file_name = file.replace('.csv', '_fixed.csv')
        table = clevercsv.wrappers.read_table(file)
        table = [data for data in table if data != []]
        clevercsv.wrappers.write_table(table, new_file_name)
        error_msg += f" fixed file created at : {new_file_name} "

    print(validation, error_msg)
    return validation, error_msg
