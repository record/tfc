#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 12 08:45:59 2023

@author: amonteil
"""

from typing_extensions import Annotated
import clevercsv
import typer


app = typer.Typer()


def cleverCSV(file: Annotated[str,
              typer.Argument(help="File name or Path/filename")],
              verbose: Annotated[bool, typer.Option()] = False):
    """
    Convert csv file into csv file with RFC4180 norm \n
    """
    new_file_name = file.replace('.csv', '_rfc4180.csv')
    table = clevercsv.wrappers.read_table(file)
    table = [data for data in table if data != []]
    try:
        clevercsv.wrappers.write_table(table, new_file_name)
    except ValueError:
        print('File not converted')
        return False
    else:
        if verbose:
            print("File converted.\nRFC4180 file created at: "
                  f"{file.replace('.csv', '_report.json')}\n")
            return True
        else:
            print('File converted')
            return True
