import setuptools

setuptools.setup(
    name="tfc",
    description="Tabular File Checker",
    version="1.0.7",
    author="Alexandre Monteil, Patrick Chabrier",
    author_email="alexandre.monteil@inrae.fr",
    url="https://forgemia.inra.fr/record/tfc/",
    packages=['tfc','tfc.script'],
    install_requires=[
        "frictionless==5.13.1",
	"typer==0.9.0",
	"clevercsv==0.8.0",
	"typing_extensions==4.5.0",
	"pytest==7.1.2",
    ],
    entry_points={
      'console_scripts': [
         'tfc=tfc.main:app',
         ],
    },    
)
