# Tabular File Checker
Travail exploratoire sur le developpement d'un outil permettant le contrôle et la validation des fichier tabulaires.
## Introduction
La boite à outils proposé dans ce projet propose deux principales fonctionnalitées :
- genschema : une fonction permettant de générer un schema (fichier contenant la structure, les metadonées ainsi que des contraintes sur la forme ou le fond d'un fichier tabulaire). Plus d'informations à retrouver sur la [documentation](https://record.pages.mia.inra.fr/tfc/genschema_stable.html)

- check : une fonction permettant de verifier si un fichier respecte un schema donné, ce qui permet de vérifier le format, les metadonnées ainsi que certaines contraintes d'un fichier. Plus d'informations à retrouver sur la [documentation](https://record.pages.mia.inra.fr/tfc/check_stable.html)


## Installation 
Pour utiliser ce projet vous pouvez le télécharger sous forme de paquet en utilisant la commande suivante :

`pip install tfc --index-url https://forgemia.inra.fr/api/v4/projects/8388/packages/pypi/simple` 


Vous pouvez aussi l'utilliser grâce à docker en utilisant l'image suivante : registry.forgemia.inra.fr/record/tfc .

Ou direcement avec la commande :

`docker run registry.forgemia.inra.fr/record/tfc <actions à effectuer>`


## Utilisation
Un manuel utilisateur et disponible [ici](https://record.pages.mia.inra.fr/tfc/users_guide_link.html).

Vous pourrez retrouver l'intégralitée de la documentation [ici](https://record.pages.mia.inra.fr/tfc/index.html)
