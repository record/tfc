#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 15:21:10 2023

@author: amonteil
"""


from tfc.script.US_1 import header_checker


def test_header_checker_1():
    assert (header_checker('test/file_test/liste_mailles_PdF1/'
                           'liste_mailles_PdF1_test.csv',
                           '[maille_safran,maille_drias]')
            == (True, {'Header error': {}, "column s name error": {}}))


def test_header_checker_2():
    assert (header_checker('test/file_test/ephemeride_systeme_jupiter/'
                           'ephemeride_systeme_jupiter.csv',
                           '[Astre,Rayon (km),Masse (kg),Demi-grand axe (km),'
                           'Période orbitale (jours)]')
            == (True, {'Header error': {}, "column s name error": {}}))


def test_header_checker_3():
    assert (header_checker('test/file_test/ephemeride_systeme_jupiter/'
                           'ephemeride_systeme_jupiter.csv',
                           '[maille_safran,maille_drias]')
           == (False,
            {'Header error': {},
             'column s name error':
                 {'Astre':
                  ' column s 1 name does not match with argument list name'
                  ' (maille_safran)',
                  'Rayon (km)':
                  ' column s 2 name does not match with argument list name'
                  ' (maille_drias)'}}))
