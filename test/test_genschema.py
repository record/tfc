#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 14:14:28 2023

@author: amonteil
"""


from tfc.script.genschema import genschema


def test_genschema_1():
    assert genschema('test/file_test/date/date.csv') == ({'name': 'date',
      'type': 'table',
      'path': 'test/file_test/date/date.csv',
      'scheme': 'file',
      'format': 'csv',
      'mediatype': 'text/csv',
      'encoding': 'utf-8',
      'hash': 'sha256:4cfc5a1053f6cb741f7b5d7a59a5bf925b922f2ed59ff7b40a2b53db1e39e53c',
      'bytes': 127,
      'fields': 2,
      'rows': 5,
      'dialect': {'csv': {'skipInitialSpace': True}},
      'schema': {'fields': {'dates': {'type': 'date', 'mandatory': False},
        'test': {'type': 'date', 'mandatory': False}}},
      'header': ['dates', 'test']},
     'test/file_test/date/date_schema.json')


def test_genschema_2():
    assert (genschema('test/file_test/ephemeride_systeme_jupiter/ephemeride_systeme_jupiter.csv')
            == ({'name': 'ephemeride_systeme_jupiter',
              'type': 'table',
              'path': 'test/file_test/ephemeride_systeme_jupiter/ephemeride_systeme_jupiter.csv',
              'scheme': 'file',
              'format': 'csv',
              'mediatype': 'text/csv',
              'encoding': 'utf-8',
              'hash': 'sha256:03a1502fe5d68b3e28a48f4b3cf984267c4799c711016ab178d9cbfd5958281f',
              'bytes': 2573,
              'fields': 5,
              'rows': 66,
              'dialect': {'csv': {'delimiter': ';'}},
              'schema': {'fields': {'Astre': {'type': 'string', 'mandatory': False},
                'Rayon (km)': {'type': 'string', 'mandatory': False},
                'Masse (kg)': {'type': 'string', 'mandatory': False},
                'Demi-grand axe (km)': {'type': 'integer', 'mandatory': False},
                'Période orbitale (jours)': {'type': 'string', 'mandatory': False}}},
              'header': ['Astre',
               'Rayon (km)',
               'Masse (kg)',
               'Demi-grand axe (km)',
               'Période orbitale (jours)']},
             'test/file_test/ephemeride_systeme_jupiter/ephemeride_systeme_jupiter_schema.json')
            )


def test_genschema_3():
    assert genschema('test/file_test/function/function.csv') == ({'name': 'function',
      'type': 'table',
      'path': 'test/file_test/function/function.csv',
      'scheme': 'file',
      'format': 'csv',
      'mediatype': 'text/csv',
      'encoding': 'utf-8',
      'hash': 'sha256:fdc34c56625d70127c6375910a5f2a95059058f317e127482e170264339f2453',
      'bytes': 91,
      'fields': 3,
      'rows': 10,
      'schema': {'fields': {'col_1': {'type': 'integer', 'mandatory': False},
        'col_2': {'type': 'integer', 'mandatory': False},
        'col_3': {'type': 'integer', 'mandatory': False}}},
      'header': ['col_1', 'col_2', 'col_3']},
     'test/file_test/function/function_schema.json')


def test_genschema_4():
    assert (genschema('test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test.csv')
            == ({'name': 'liste_mailles_pdf1_test',
              'type': 'table',
              'path': 'test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test.csv',
              'scheme': 'file',
              'format': 'csv',
              'mediatype': 'text/csv',
              'encoding': 'utf-8',
              'hash': 'sha256:5fb42a8083f4a551d438cc46919ca573381b04a96211c043a82820cd039cf635',
              'bytes': 306,
              'fields': 2,
              'rows': 25,
              'dialect': {'csv': {'delimiter': ';'}},
              'schema': {'fields': {'maille_safran': {'type': 'integer',
                 'mandatory': False},
                'maille_drias': {'type': 'integer', 'mandatory': False}}},
              'header': ['maille_safran', 'maille_drias']},
             'test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test_schema.json')
            )


def test_genschema_5():  # begin date inference in atomic constraints
    assert (genschema('test/file_test/date/date.csv',
                      atomic='[dates,begin_date_field]')
            == ({'name': 'date',
              'type': 'table',
              'path': 'test/file_test/date/date.csv',
              'scheme': 'file',
              'format': 'csv',
              'mediatype': 'text/csv',
              'encoding': 'utf-8',
              'hash': 'sha256:4cfc5a1053f6cb741f7b5d7a59a5bf925b922f2ed59ff7b40a2b53db1e39e53c',
              'bytes': 127,
              'fields': 2,
              'rows': 5,
              'dialect': {'csv': {'skipInitialSpace': True}},
              'schema': {'fields': {'dates': {'type': 'date', 'mandatory': False},
                'test': {'type': 'date', 'mandatory': False}}},
              'header': ['dates', 'test'],
              'constraints': {'atomic': [('dates', 'begin_date_field')]}},
             'test/file_test/date/date_schema.json')
            )


def test_genschema_1_bis():
    assert genschema('test/file_test/date/date.csv') == ({'name': 'date',
      'type': 'table',
      'path': 'test/file_test/date/date.csv',
      'scheme': 'file',
      'format': 'csv',
      'mediatype': 'text/csv',
      'encoding': 'utf-8',
      'hash': 'sha256:4cfc5a1053f6cb741f7b5d7a59a5bf925b922f2ed59ff7b40a2b53db1e39e53c',
      'bytes': 127,
      'fields': 2,
      'rows': 5,
      'dialect': {'csv': {'skipInitialSpace': True}},
      'schema': {'fields': {'dates': {'type': 'date', 'mandatory': False},
        'test': {'type': 'date', 'mandatory': False}}},
      'header': ['dates', 'test']},
     'test/file_test/date/date_schema.json')


def test_genschema_6():  # lower than inference in atomic constraints
    assert (genschema('test/file_test/function/function.csv',
                      atomic='[col_1,<,15]')
            ==
            ({'name': 'function',
              'type': 'table',
              'path': 'test/file_test/function/function.csv',
              'scheme': 'file',
              'format': 'csv',
              'mediatype': 'text/csv',
              'encoding': 'utf-8',
              'hash': 'sha256:fdc34c56625d70127c6375910a5f2a95059058f317e127482e170264339f2453',
              'bytes': 91,
              'fields': 3,
              'rows': 10,
              'schema': {'fields': {'col_1': {'type': 'integer', 'mandatory': False},
                'col_2': {'type': 'integer', 'mandatory': False},
                'col_3': {'type': 'integer', 'mandatory': False}}},
              'header': ['col_1', 'col_2', 'col_3'],
              'constraints': {'atomic': [('col_1', '<', '15')]}},
             'test/file_test/function/function_schema.json')
            )


def test_genschema_7():  # greater than inference in atomic constraints
    assert (genschema('test/file_test/function/function.csv',
                      atomic='[col_2,>,1]')
            ==
            ({'name': 'function',
              'type': 'table',
              'path': 'test/file_test/function/function.csv',
              'scheme': 'file',
              'format': 'csv',
              'mediatype': 'text/csv',
              'encoding': 'utf-8',
              'hash': 'sha256:fdc34c56625d70127c6375910a5f2a95059058f317e127482e170264339f2453',
              'bytes': 91,
              'fields': 3,
              'rows': 10,
              'schema': {'fields': {'col_1': {'type': 'integer', 'mandatory': False},
                'col_2': {'type': 'integer', 'mandatory': False},
                'col_3': {'type': 'integer', 'mandatory': False}}},
              'header': ['col_1', 'col_2', 'col_3'],
              'constraints': {'atomic': [('col_2', '>', '1')]}},
             'test/file_test/function/function_schema.json')
            )


def test_genschema_8():  # lower than inference in columns constraints
    assert(genschema('test/file_test/function/function.csv',
                      columns='[col_1,<,col_2]')
           ==
           ({'name': 'function',
             'type': 'table',
             'path': 'test/file_test/function/function.csv',
             'scheme': 'file',
             'format': 'csv',
             'mediatype': 'text/csv',
             'encoding': 'utf-8',
             'hash': 'sha256:fdc34c56625d70127c6375910a5f2a95059058f317e127482e170264339f2453',
             'bytes': 91,
             'fields': 3,
             'rows': 10,
             'schema': {'fields': {'col_1': {'type': 'integer', 'mandatory': False},
               'col_2': {'type': 'integer', 'mandatory': False},
               'col_3': {'type': 'integer', 'mandatory': False}}},
             'header': ['col_1', 'col_2', 'col_3'],
             'constraints': {'columns': [('col_1', '<', 'col_2')]}},
            'test/file_test/function/function_schema.json')
           )


def test_genschema_9():  # greater than inference in columns constraints
    assert(genschema('test/file_test/function/function.csv',
                     columns='[col_3,>,col_1]')
           ==
           ({'name': 'function',
             'type': 'table',
             'path': 'test/file_test/function/function.csv',
             'scheme': 'file',
             'format': 'csv',
             'mediatype': 'text/csv',
             'encoding': 'utf-8',
             'hash': 'sha256:fdc34c56625d70127c6375910a5f2a95059058f317e127482e170264339f2453',
             'bytes': 91,
             'fields': 3,
             'rows': 10,
             'schema': {'fields': {'col_1': {'type': 'integer', 'mandatory': False},
               'col_2': {'type': 'integer', 'mandatory': False},
               'col_3': {'type': 'integer', 'mandatory': False}}},
             'header': ['col_1', 'col_2', 'col_3'],
             'constraints': {'columns': [('col_3', '>', 'col_1')]}},
            'test/file_test/function/function_schema.json')
           )


def test_genschema_10():  # functions infereance in columns constraints
    assert(genschema('test/file_test/function/function.csv',
                      columns='[col_1,function,2*x,col_2]')
           ==
           ({'name': 'function',
             'type': 'table',
             'path': 'test/file_test/function/function.csv',
             'scheme': 'file',
             'format': 'csv',
             'mediatype': 'text/csv',
             'encoding': 'utf-8',
             'hash': 'sha256:fdc34c56625d70127c6375910a5f2a95059058f317e127482e170264339f2453',
             'bytes': 91,
             'fields': 3,
             'rows': 10,
             'schema': {'fields': {'col_1': {'type': 'integer', 'mandatory': False},
               'col_2': {'type': 'integer', 'mandatory': False},
               'col_3': {'type': 'integer', 'mandatory': False}}},
             'header': ['col_1', 'col_2', 'col_3'],
             'constraints': {'columns': [('col_1',
                'function',
                '2*x',
                'col_2')]}},
            'test/file_test/function/function_schema.json')
           )



def test_genschema_11():  #several inference in constraints
    assert (genschema('test/file_test/function/function.csv',
                  atomic='[dates,begin_date_field];[col_1,<,15];[col_2,>,1]',
                  columns='[col_1,<,col_2];[col_3,>,col_1];[col_1,function, 2*x,col_2]')
            ==
            ({'name': 'function',
              'type': 'table',
              'path': 'test/file_test/function/function.csv',
              'scheme': 'file',
              'format': 'csv',
              'mediatype': 'text/csv',
              'encoding': 'utf-8',
              'hash': 'sha256:fdc34c56625d70127c6375910a5f2a95059058f317e127482e170264339f2453',
              'bytes': 91,
              'fields': 3,
              'rows': 10,
              'schema': {'fields': {'col_1': {'type': 'integer', 'mandatory': False},
                'col_2': {'type': 'integer', 'mandatory': False},
                'col_3': {'type': 'integer', 'mandatory': False}}},
              'header': ['col_1', 'col_2', 'col_3'],
              'constraints': {'atomic': [('dates', 'begin_date_field'),
                ('col_1', '<', '15'),
                ('col_2', '>', '1')],
               'columns': [('col_1', '<', 'col_2'),
                ('col_3', '>', 'col_1'),
                ('col_1', 'function', '2*x', 'col_2')]}},
             'test/file_test/function/function_schema.json')
            )


def test_genschema_12():  # 'in' inference in atomic constraints
    assert (genschema('test/file_test/test_in_file/file_a.csv',
                      atomic='[col_1,in]')
            ==
            ({'name': 'file_a',
              'type': 'table',
              'path': 'test/file_test/test_in_file/file_a.csv',
              'scheme': 'file',
              'format': 'csv',
              'mediatype': 'text/csv',
              'encoding': 'utf-8',
              'hash': 'sha256:578b94e6e24808f84063f2309c0c645546c97ed41512c315ff9f7523b1921409',
              'bytes': 50,
              'fields': 3,
              'rows': 1,
              'schema': {'fields': {'col_1': {'type': 'string', 'mandatory': False},
                'col_2': {'type': 'string', 'mandatory': False},
                'col_3': {'type': 'string', 'mandatory': False}}},
              'header': ['col_1', 'col_2', 'col_3'],
              'constraints': {'atomic': [('col_1', 'in')]}},
             'test/file_test/test_in_file/file_a_schema.json')
            )
