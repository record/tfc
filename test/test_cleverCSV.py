#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 12 14:19:00 2023

@author: amonteil
"""


from tfc.script.cleverCSV import cleverCSV


def test_cleverCSV_1():
    ref = open('test/file_test/liste_mailles_PdF1/'
               'liste_mailles_PdF1_test_ref.csv', 'r')
    ref = ref.read()
    cleverCSV('test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test.csv')
    test = open('test/file_test/liste_mailles_PdF1/'
                'liste_mailles_PdF1_test_rfc4180.csv')
    test = test.read()
    assert ref == test


def test_cleverCSV_2():
    ref = open('test/file_test/ephemeride_systeme_jupiter/'
               'ephemeride_systeme_jupiter_ref.csv')
    ref = ref.read()
    cleverCSV('test/file_test/ephemeride_systeme_jupiter/'
              'ephemeride_systeme_jupiter.csv')
    test = open('test/file_test/ephemeride_systeme_jupiter/'
                'ephemeride_systeme_jupiter_rfc4180.csv')
    test = test.read()
    assert ref == test
