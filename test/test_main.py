#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 22 10:20:30 2023

@author: amonteil
"""

from typer.testing import CliRunner
from tfc.main import app

runner = CliRunner()


def test_main_check_1():
    result = runner.invoke(
        app,
        ['check',
         'test/file_test/ephemeride_systeme_jupiter/'
         'ephemeride_systeme_jupiter.csv',
         'test/file_test/ephemeride_systeme_jupiter/'
         'ephemeride_systeme_jupiter_schema.json'
         ]
        )
    assert result.exit_code == 0
    assert "No error" in result.stdout


def test_main_check_2():
    result = runner.invoke(
        app,
        ['check',
         'test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test.csv',
         'test/file_test/liste_mailles_PdF1/'
         'liste_mailles_PdF1_test_schema.json'
         ]
        )
    assert result.exit_code == 0
    assert "No error" in result.stdout


def test_main_cleverCSV_1():
    result = runner.invoke(
        app,
        ['clevercsv',
         'test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test.csv',
         ])
    with open('test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test_ref.csv', 'r') as ref:
        ref = ref.read()
    with open('test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test_rfc4180.csv', 'r') as test:
        test = test.read()
    assert result.exit_code == 0
    assert "File converted" in result.stdout
    assert ref == test


def test_main_cleverCSV_2():
    result = runner.invoke(
        app,
        ['clevercsv',
         'test/file_test/ephemeride_systeme_jupiter/ephemeride_systeme_jupiter.csv',
         ])
    with open('test/file_test/ephemeride_systeme_jupiter/ephemeride_systeme_jupiter_ref.csv', 'r') as ref:
        ref = ref.read()
    with open('test/file_test/ephemeride_systeme_jupiter/ephemeride_systeme_jupiter_rfc4180.csv') as test:
        test = test.read()
    assert result.exit_code == 0
    assert "File converted" in result.stdout
    assert ref == test


def test_main_csv_checker_1():
    result = runner.invoke(
        app,
        ['csv-checker',
         'test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test.csv'
         ])
    assert result.exit_code == 0
    assert ('False  wrong delimiter  fixed file created at : '
            'test/file_test/liste_mailles_PdF1/'
            'liste_mailles_PdF1_test_fixed.csv'
            in result.stdout)
