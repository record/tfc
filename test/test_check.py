#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 17 13:34:48 2023

@author: amonteil
"""


from tfc.script.check import check


def test_check_1():
    assert (check('test/file_test/ephemeride_systeme_jupiter'
                  '/ephemeride_systeme_jupiter_ref.csv',
                  'test/file_test/ephemeride_systeme_jupiter'
                  '/ephemeride_systeme_jupiter_ref_schema.json')
            == {})


def test_check_2():
    assert (check('test/file_test/liste_mailles_PdF1/'
                  'liste_mailles_PdF1_test.csv',
                  'test/file_test/liste_mailles_PdF1/'
                  'liste_mailles_PdF1_test_schema.json')
            == {})


def test_check_3():
    assert (check('test/file_test/ephemeride_systeme_jupiter'
                  '/ephemeride_systeme_jupiter_ref.csv',
                  'test/file_test/liste_mailles_PdF1/'
                  'liste_mailles_PdF1_test_schema.json')
            == {'rows': {'wrong file rows':
                         'file rows (66) does not match with ref (25)'},
                'extra fields': ['Astre',
                                 'Rayon (km)',
                                 'Masse (kg)',
                                 'Demi-grand axe (km)',
                                 'Période orbitale (jours)'],
                'missing fields': ['maille_safran', 'maille_drias'],
                'fields': {'wrong file fields number ':
                           'file fields number(5) '
                           'does not match with ref (2)'},
                'mandatory field missing': [],
                'non mandatory field missing':
                    ['maille_safran', 'maille_drias']})


def test_check_4():
    assert (
        check(
            'test/file_test/date/date.csv',
            'test/file_test/date/date_schema.json') == {})


def test_check_5():  # date inference
    assert (check('test/file_test/date/date.csv',
                  'test/file_test/date/date_schema_ref.json')
            ==
            ({'constraints': {'atomic_constraints': ['inconsistent dates: '
                                                     '2022-08-09 in field test'
                                                     ' is before date min: '
                                                     '2023-01-01 in field: '
                                                     'dates',
                                                     'inconsistent dates: '
                                                     '2022-08-10 in field test'
                                                     ' is before date min: '
                                                     '2023-01-02 in field: '
                                                     'dates',
                                                     'inconsistent dates: '
                                                     '2022-08-11 in field test'
                                                     ' is before date min: '
                                                     '2023-01-03 in field: '
                                                     'dates',
                                                     'inconsistent dates: '
                                                     '2022-08-12 in field test'
                                                     ' is before date min: '
                                                     '2023-01-04 in field: '
                                                     'dates',
                                                     'inconsistent dates: '
                                                     '2022-08-13 in field test'
                                                     ' is before date min: '
                                                     '2023-01-05 in field: '
                                                     'dates']}}))


def test_check_6():  # function inference
    assert (
        check(
            'test/file_test/function/function.csv',
            'test/file_test/function/function_schema_ref.json') == {
            'constraints': {
                'columns_constraints': [
                    'col_1 are not matching col_2 formula (2*x) at row [5]',
                    'col_1 are not matching col_3 formula (x**2) at row [5]']
                }})

