#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 15:20:51 2023

@author: amonteil
"""


from tfc.script.US_0 import csv_checker


def test_csv_checker_1():
    assert (csv_checker(
        'test/file_test/liste_mailles_PdF1/liste_mailles_PdF1_test.csv')
        == (False,
            ' wrong delimiter  fixed file created at : '
            'test/file_test/liste_mailles_PdF1/'
            'liste_mailles_PdF1_test_fixed.csv ')
        )
