FROM python:3.8

WORKDIR tfc

RUN pip install tfc --index-url https://forgemia.inra.fr/api/v4/projects/8388/packages/pypi/simple

ENTRYPOINT ["tfc"]
CMD ["--help"]
