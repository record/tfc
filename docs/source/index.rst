.. tfc documentation master file, created by
   sphinx-quickstart on Fri Jun 16 15:11:27 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tfc's documentation!
===============================


.. toctree::
   maxdepth: 5
   caption: Contents
   ./readme_link
   ./genschema
   ./genschema_stable
   ./check
   ./check_stable
   ./utils
   ./users_guide_link

   .. automodule:: tfc.utils
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
