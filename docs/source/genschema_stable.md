##  Genschema (stable)
This function allows you to create a schema from a file, and exporting it in a json file. It can also add metadatas and constraints to your file.

To do so, use the following command :

```
tfc genschema genschema path/to/your/file/filename
```

Several inferences are available:

- --mandatories: list containning mandatories fields name.
	
- --atomic: list containning atomic constraints, 
			      		  
- --columns: list containning columns constraints
					  	   
- --schema-name: Where you want the schema to be created
	
- --verbose: if enabled show you the schema in the terminal
	
- --help
	
	
###  mandatories
This options specify which fields are mandatories. The input is a string containing mandatories fields name separated by a coma. 

Here's an example :

```
tfc genschema genschema path/to/your/file/filename --mandatories='field_name_1,... ,field_name_n'
```
	
	
###   atomic
This option add constraints one a single field. The input is a string contaning lists, which length could be 2 or 3. The elements of the list are separated by a coma and the lists are separated by a semicolon .

The constraints available are : 

- **'begin_date_field'**: specify which date field should contain the oldest dates.

*Example of the list* : [field_name,begin_date_field]
	
- **'in'**: in a field only containning file name, specify that those file should be present in the directory of the file you are 
working on.

*Example of the list* : [field_name,in]

- **'>'** : in a numerical field, specify that all the values of the column should be greater than a value.

*Example of the list* : [field_name,>,value]
	
	
- **'<'** : in a numerical field, specify that all the values of the column should be lower than a value.

*Example of the list* : [field_name,<,value]
	

Here's an example of several atomic-constraints together :

```
tfc genschema genschema path/to/your/file/filename --atomic='[field_name_1,begin_date_field];[field_name_3,in];[field_name_2,>,125];[field_name_5,<,18.5]'
```
     
	
###   columns
This option add constraints between two fields. The input is a string containing lists which length could be 3 or 4.

The constraints available are : 

- **'>'** : in a numerical field, specify that all the value of given column are greater than another given column.

*Example of the list* : [field_name_1,>,field_name_2]

In this case, we specify that all field_name_1's value should be greater than field_name_2's value.
		
- **'>'** : in a numerical field, specify that all the value of given column are lower than another given column.

*Example of the list* : [field_name_1,<,field_name_2]

In this case, we specify that all field_name_1's value should be lower than field_name_2's value.
		
- **'function'** : in a numerical field, specify that all the value of given column are functions of another given column.

*Example of the list* :	[field_name_1,function,expression,field_name_2]

In this case, we specify that all field_name_2's value should be equal to  expression(field_name_1's) value.

*Example of expression* : 2*x, 3*x+4, x**2/2, ...

Here's an example of several columns-constraints together:

```
tfc genschema genschema path/to/your/file/filename --columns='[field_name_1,<,field_name_2];[field_name_3,>,field_name_4];[field_name_2,function,2*x+1,field_name_6]'
```
  
  
###   schema-name
This option specify where the schema, in json format, will be generated.

*Example* : 
```
tfc genschema genschema path/to/your/file/filename --schema_name=new/path/to/your/schema/my_schema.json
```
  
  
###   verbose
If this option is enabled, will show you the schema in the terminal.
Default is False.
*Example*:
```
tfc genschema genschema path/to/your/file/filename --verbose
```
  
