##  Check (stable)
This function allow you to check if a tabular file follows a given schema. If the tabular file does not follow the given schema, a json format report will be generated in the tabular file folder.

To do so, use the following command :
```
tfc check check path/to/your/file/filename path/to/your/schema/schema.json
```

Several options are available :
- --ordered
- --extra_col
- --verbose
- --help


###  ordered
If this option is enabled, the function check if the file fields are in the same order than the schema fields. 
Default is False
*Example* :
```
tfc check check path/to/your/file/filename path/to/your/schema/schema.json --ordered
```


###  extra_col
If this option is enabled, the function will authorize the presence of extra field/column. If not the function will list the forbidden extra fields in the error report.
Default is True.
*Example* :
```
tfc check check path/to/your/file/filename path/to/your/schema/schema.json --no-extra_col
```


###  verbose
If this option is enabled, will show you the error report in the terminal.
Default is False.
*Example* :
```
tfc check check path/to/your/file/filename path/to/your/schema/schema.json --verbose
```



