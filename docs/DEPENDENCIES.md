# About dependencies and tools

## Conda environment 

### Anaconda-Navigator 2.4.0
https://docs.anaconda.com/free/anaconda/install/index.html

to check if you have it: 

`anaconda-navigator --version`


### Spyder 5.4.3
(pre-installed with Anaconda)

if not : 

`conda install spyder`

### Python 3.10.9
(pre-installed with Anaconda, on Spyder IDE)


### Frictionless 5.13.1 
to check if you have it:

`frictionless --version`

if you have it, it should return:

`5.13.1`

if not:

`conda install -c conda-forge frictionless=5.13.1`


### typer 0.9.0
`pip install typer[full]`

###  clevercsv 0.8.0
`pip instal clevercsv`

### typing_extensions 4.5.0
(pre-installed with Anaconda)

### pytest 7.1.2
(pre-installed with Anaconda)
  



## Other environment

### Frictionless 5.13.1 
to check if you have it:

`frictionless --version`

if you have it, it should return:

`5.13.1`

if not:

`pip install frictionless==5.13.1`


### typer 0.9.0
`pip install typer[full]`

###  clevercsv 0.8.0
`pip install clevercsv`

### typing_extensions 4.5.0
`pip install typing_extensions==4.5.0`

### pytest 7.1.2
`pip install pytest==7.1.2`  
`
